const tabs = document.querySelectorAll('[data-tab-target]')
const tabContent = document.querySelectorAll('[data-tab-content]')

tabs.forEach(tab => {
    tab.addEventListener('click', () => {
        const target = document.querySelector(tab.dataset.tabTarget)
        tabContent.forEach(tabContent => {
            tabContent.classList.remove('active')
        })
        tabs.forEach(tab => {
            tab.classList.remove('active')
        })
        target.classList.add('active')
        tab.classList.add('active')
    })
})

function ibg() {
	let ibg = document.querySelectorAll(".ibg");
	for (var i = 0; i < ibg.length; i++) {
		if (ibg[i].querySelector('img')) {
			ibg[i].style.backgroundImage = 'url(' + ibg[i].querySelector('img').getAttribute('src') + ')';
		}
	}
}
ibg();

// function mainblock() {
//     var h = $(window).outerHeight();
//     $('.main').css('min-height', h);
//     $('.main-body-content').css('min-height', h)
// }
// mainblock()

function textContent() {
    const textCarousel = document.querySelectorAll('.hotel-splide-body-content__contact')
    for (let i = 0; i < textCarousel.length; i++) {
        const element = textCarousel[i];
        if (element.textContent.trim().length < 255) {
            element.classList.add('cut-text')
        }
    }
}

const isDevice = {
    Android: function() {return navigator.userAgent.match(/Android/i);},
    BlackBerry: function() {return navigator.userAgent.match(/BlackBerry/i);},
    iOS: function() {return navigator.userAgent.match(/iPhone|iPad|iPod/i);},
    Opera: function() {return navigator.userAgent.match(/Opera Mini/i);},
    Windows: function() {return navigator.userAgent.match(/IEMobile/i);},
    any: function() {return (isDevice.Android() || isDevice.BlackBerry() || isDevice.iOS() || isDevice.Opera() || isDevice.Windows());},
}

const body = document.querySelector('body')

function menuDropdown() {
    body.classList.add('touch')
    const arrow = document.querySelectorAll('.arrow')
    const menuItem = document.querySelectorAll('.menu__list_item')
    for (let i = 0; i < arrow.length; i++) {
        let thisLink = arrow[i].previousElementSibling;
        let subMenu = arrow[i].nextElementSibling;
        let thisArrow = arrow[i];
        thisLink.classList.add('parent')
        menuItem[i].addEventListener('click', () => {
            subMenu.classList.toggle('open')
            thisArrow.classList.toggle('active')
        })
    }
}

menuDropdown()

function tempFunc() {
    let ibg = document.querySelectorAll(".temp-class");

	for (var i = 0; i < ibg.length; i++) {
        // console.log('123123123');
		if (ibg[i].querySelector('.tp-bgimg')) {
            // console.log('asdasd');
			ibg[i].style.backgroundImage = 'url(' + ibg[i].querySelector('.tp-bgimg').getAttribute('src') + ')';
		}
	}
}

tempFunc()

let startDate = new Date();
let endDate = new Date();

const picker = new Litepicker({
    element: document.getElementById('litepicker'),
    elementEnd: document.getElementById('end-date'),
    singleMode: false,
    allowRepick: true,
    numberOfMonths: 2,
    numberOfColumns: 2,
    lang: 'ru-RU',
    format: 'YYYY-MM-DD',
    startDate: startDate,
    endDate: endDate,
    onShow: function() {
        console.log('фыывфыв');
    },
    onSelect: function(startDate) {
        var d1 = startDate;
        var day1 = d1.getDate();

        document.getElementById("litepicker").innerHTML = day1;
     }
});

// var Carousel = {
//     width: 100,     // Images are forced into a width of this many pixels.
//     numVisible: 2,  // The number of images visible at once.
//     duration: 600,  // Animation duration in milliseconds.
//     padding: 2      // Vertical padding around each image, in pixels.
// };

// function rotateForward() {
//     var carousel = Carousel.carousel,
//         children = carousel.children,
//         firstChild = children[0],
//         lastChild = children[children.length - 1];
//     carousel.insertBefore(lastChild, firstChild);
// }
// function rotateBackward() {
//     var carousel = Carousel.carousel,
//         children = carousel.children,
//         firstChild = children[0],
//         lastChild = children[children.length - 1];
//     carousel.insertBefore(firstChild, lastChild.nextSibling);
// }

// function animate(begin, end, finalTask) {
//     var wrapper = Carousel.wrapper,
//         carousel = Carousel.carousel,
//         change = end - begin,
//         duration = Carousel.duration,
//         startTime = Date.now();
//     carousel.style.top = begin + 'px';
//     var animateInterval = window.setInterval(function () {
//         var t = Date.now() - startTime;
//         if (t >= duration) {
//             window.clearInterval(animateInterval);
//             finalTask();
//             return;
//         }
//         t /= (duration / 2);
//         var top = begin + (t < 1 ? change / 2 * Math.pow(t, 3) :
//             change / 2 * (Math.pow(t - 2, 3) + 2));
//         carousel.style.top = top + 'px';
//     }, 1000 / 60);
// }

// window.onload = function () {
//     document.getElementById('spinner').style.display = 'none';
//     var carousel = Carousel.carousel = document.getElementById('carousel'),
//         images = carousel.getElementsByTagName('img'),
//         numImages = images.length,
//         imageWidth = Carousel.width,
//         aspectRatio = images[0].width / images[0].height,
//         imageHeight = imageWidth / aspectRatio,
//         padding = Carousel.padding,
//         rowHeight = Carousel.rowHeight = imageHeight + 2 * padding;
//     carousel.style.width = imageWidth + 'px';
//     for (var i = 0; i < numImages; ++i) {
//         var image = images[i],
//             frame = document.createElement('div');
//         frame.className = 'pictureFrame';
//         var aspectRatio = image.offsetWidth / image.offsetHeight;
//         image.style.width = frame.style.width = imageWidth + 'px';
//         image.style.height = imageHeight + 'px';
//         image.style.paddingTop = padding + 'px';
//         image.style.paddingBottom = padding + 'px';
//         frame.style.height = rowHeight + 'px';
//         carousel.insertBefore(frame, image);
//         frame.appendChild(image);
//     }
//     Carousel.rowHeight = carousel.getElementsByTagName('div')[0].offsetHeight;
//     carousel.style.height = Carousel.numVisible * Carousel.rowHeight + 'px';
//     carousel.style.visibility = 'visible';
//     var wrapper = Carousel.wrapper = document.createElement('div');
//     wrapper.id = 'carouselWrapper';
//     console.log('Offset width', carousel.offsetWidth + 'px', 'Offset height', carousel.offsetHeight + 'px');
//     wrapper.style.width = '1140px';
//     wrapper.style.height = '314px';
//     carousel.parentNode.insertBefore(wrapper, carousel);
//     wrapper.appendChild(carousel);
//     var prevButton = document.getElementById('prev'),
//         nextButton = document.getElementById('next');
//     prevButton.onclick = function () {
//         prevButton.disabled = nextButton.disabled = true;
//         rotateForward();
//         animate(-Carousel.rowHeight, 0, function () {
//             carousel.style.top = '0';
//             prevButton.disabled = nextButton.disabled = false;
//         });
//     };
//     nextButton.onclick = function () {
//         prevButton.disabled = nextButton.disabled = true;
//         animate(0, -Carousel.rowHeight, function () {
//             rotateBackward();
//             carousel.style.top = '0';
//             prevButton.disabled = nextButton.disabled = false;
//         });
//     };
// };