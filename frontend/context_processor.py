from site_management.models import ToursHeader, MainImage


def all_see(request):
    path_ru = request.path.replace('/en/', '/ru/').replace('/uz/', '/ru/')
    path_en = request.path.replace('/ru/', '/en/').replace('/uz/', '/en/')
    path_uz = request.path.replace('/en/', '/uz/').replace('/ru/', '/uz/')
    header = MainImage.objects.first()
    return {
        'path_ru': path_ru,
        'path_en': path_en,
        'path_uz': path_uz,
        'header': header,
    }
