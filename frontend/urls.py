from django.urls import path
from .views import HomeView, DetailToursView, TourListView, HotelListView, QuestionListView, UzbekistanTourListView, \
    CountryListView

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    # path('about-us/', AboutUsView.as_view(), name='about-us'),
    # path('news/', NewsView.as_view(), name='news'),
    path('tour/<int:pk>', DetailToursView.as_view(), name='tour-detail'),
    path('tours/', TourListView.as_view(), name='tour-list'),
    path('uzbekistan-tours/', UzbekistanTourListView.as_view(), name='uzbekistan-tour-list'),
    path('hotels/', HotelListView.as_view(), name='hotel-list'),
    path('questions/', QuestionListView.as_view(), name='question-list'),
    path('country/', CountryListView.as_view(), name='country-list'),
]
