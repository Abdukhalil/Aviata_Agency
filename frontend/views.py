from decimal import Decimal

from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import TemplateView, ListView, DetailView
from paycomuz.views import MerchantAPIView
from paycomuz import Paycom
from django.urls import path
from core.helpers import send_email_message
from core.models import Tour, Slider, From, To, Country, Airline, HotelTo, Hotel, Question, Region

# Home View домашняя страница
from site_management.models import BestTourText, AboutUsText, ContactUsText, Visa, Workers, MainImage


class HomeView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        sliders = Slider.objects.all()
        context['sliders'] = sliders

        context['hot_tours'] = Tour.objects.filter(is_hot=True, is_active=True)
        context['from'] = From.objects.order_by('-country__title')
        context['to'] = To.objects.order_by('-country__title')
        context['best_tours_text'] = BestTourText.objects.first()
        context['about_us'] = AboutUsText.objects.first()
        context['contact_us_text'] = ContactUsText.objects.first()
        context['airlines'] = Airline.objects.all()
        context['to_hotels'] = HotelTo.objects.all()
        context['visa'] = Visa.objects.first()
        context['workers'] = Workers.objects.all()
        context['main_image'] = MainImage.objects.first()
        return context

    def post(self, request):
        name = request.POST.get('name', None)
        email = request.POST.get('email', None)
        content = request.POST.get('content', None)
        if name and email and content:
            send_email_message(name, content, email)
        return redirect(reverse('home'))


class DetailToursView(DetailView):
    template_name = 'tour-detail.html'
    model = Tour
    context_object_name = 'tour'

    def post(self, request, pk):
        paycom_type = request.POST.get('type', None)
        obj = Tour.objects.get(id=pk)
        if paycom_type:
            paycom = Paycom()
            paycom.create_initialization(amount=Decimal(obj.amount), order_id='197', return_url=request.path)
            print('yes')
        name = request.POST.get('name', None)
        email = request.POST.get('email', None)
        content = request.POST.get('content', None)
        if name and email and content:
            send_email_message(name, content, email)
        return redirect(reverse('tour-detail', kwargs={'pk': pk}))


class UzbekistanTourListView(ListView):
    template_name = 'tours.html'
    model = Tour
    context_object_name = 'tours'
    paginate_by = 3

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(UzbekistanTourListView, self).get_context_data(**kwargs)
        context['countries'] = Country.objects.prefetch_related('to', 'froms')
        context['search'] = self.request.GET.get('search', "")
        context['to'] = self.request.GET.get('to', "")
        context['froms'] = self.request.GET.get('froms', "")
        return context

    def get_queryset(self):
        search = self.request.GET.get('search', "")
        froms = self.request.GET.get('from', None)
        to = self.request.GET.get('to', None)
        start_date = self.request.GET.get('start_date', None)
        end_date = self.request.GET.get('end_date', None)
        transfer = self.request.GET.get('transfer', None)
        airline = self.request.GET.get('airline', None)
        have_a_car = self.request.GET.get('have_a_car', None)
        have_a_boat = self.request.GET.get('have_a_boat', None)
        query = super().get_queryset()
        query_kwargs = {}
        if search:
            return query.filter(global_title__icontains=search)
        if to:
            query_kwargs['to_id__in'] = [to]
        if froms:
            query_kwargs['froms_id__in'] = [froms]
        if start_date:
            query_kwargs['start_date'] = start_date
        if end_date:
            query_kwargs['end_date'] = end_date
        if transfer:
            query_kwargs['transfer'] = transfer
        if airline:
            query_kwargs['airline'] = airline
        if have_a_car:
            if have_a_car == 'yes':
                have_a_car = True
            else:
                have_a_car = False
            query_kwargs['have_a_car'] = have_a_car
        if have_a_boat:
            if have_a_boat == 'yes':
                have_a_boat = True
            else:
                have_a_boat = False
            query_kwargs['have_a_boat'] = have_a_boat
        if not query_kwargs:
            return query.filter(in_uzbekistan=True)
        return query.filter(**query_kwargs)


class TourListView(ListView):
    template_name = 'tours.html'
    model = Tour
    context_object_name = 'tours'
    paginate_by = 3

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(TourListView, self).get_context_data(**kwargs)
        context['countries'] = Country.objects.prefetch_related('to', 'froms')
        context['search'] = self.request.GET.get('search', "")
        context['to'] = self.request.GET.get('to', "")
        context['froms'] = self.request.GET.get('froms', "")
        return context

    def get_queryset(self):
        search = self.request.GET.get('search', "")
        froms = self.request.GET.get('from', None)
        to = self.request.GET.get('to', None)
        region = self.request.GET.get('region', None)
        # start_date = self.request.GET.get('start_date', None)
        dates = self.request.GET.get('dates', None)
        if dates:
            dates = dates.split()
        # end_date = self.request.GET.get('end_date', None)
        transfer = self.request.GET.get('transfer', None)
        airline = self.request.GET.get('airline', None)
        have_a_car = self.request.GET.get('have_a_car', None)
        have_a_boat = self.request.GET.get('have_a_boat', None)
        tourists = self.request.GET.get('tourists', None)
        query = super().get_queryset()
        query_kwargs = {}
        if search:
            return query.filter(global_title__icontains=search)
        if to:
            query_kwargs['to_id__in'] = [to]
        if froms:
            query_kwargs['froms_id__in'] = [froms]
        if dates:
            print(dates)
            if dates[0]:
                query_kwargs['start_date'] = dates[0]
            if dates[2]:
                query_kwargs['end_date'] = dates[2]
        if transfer:
            query_kwargs['transfer'] = transfer
        if airline:
            query_kwargs['airline'] = airline
        if region:
            query_kwargs['to__country__region_id'] = region
        if tourists:
            query_kwargs['tourists'] = tourists
        if have_a_car:
            if have_a_car == 'yes':
                have_a_car = True
            else:
                have_a_car = False
            query_kwargs['have_a_car'] = have_a_car
        if have_a_boat:
            if have_a_boat == 'yes':
                have_a_boat = True
            else:
                have_a_boat = False
            query_kwargs['have_a_boat'] = have_a_boat
        if not query_kwargs:
            return query.filter()
        return query.filter(**query_kwargs)


class HotelListView(ListView):
    template_name = 'hotels.html'
    model = Hotel
    context_object_name = 'hotels'
    paginate_by = 3

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(HotelListView, self).get_context_data(**kwargs)
        context['countries'] = Country.objects.prefetch_related('to', 'froms')
        context['search'] = self.request.GET.get('search', "")
        context['hotel_to'] = HotelTo.objects.all()
        return context

    def get_queryset(self):
        to = self.request.GET.get('to', None)
        start_date = self.request.GET.get('start_date', None)
        end_date = self.request.GET.get('end_date', None)
        people_count = self.request.GET.get('people_count', None)
        search = self.request.GET.get('search', None)
        query = super().get_queryset()
        query_kwargs = {}
        if search:
            return query.filter(title__icontains=search)
        if to:
            query_kwargs['to_id__in'] = [to]
        if start_date:
            query_kwargs['start_date'] = start_date
        if end_date:
            query_kwargs['end_date'] = end_date
        if people_count:
            query_kwargs['people_count'] = people_count
        q = query.filter(**query_kwargs)
        return q


class QuestionListView(ListView):
    template_name = 'cards.html'
    model = Question
    context_object_name = 'cards'
    paginate_by = 3


class CountryListView(ListView):
    template_name = 'country.html'
    model = Region
    context_object_name = 'regions'

    def get_context_data(self, **kwargs):
        context = super(CountryListView, self).get_context_data(**kwargs)

        return context



