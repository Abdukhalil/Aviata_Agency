from django.core.exceptions import ValidationError
from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils import timezone

from site_management.models import SingletonModel


class Region(models.Model):
    title = models.CharField(
        verbose_name='Название региона',
        max_length=255
    )
    icon = models.FileField(
        verbose_name='Иконка',
        upload_to='country_icons/'
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Регион (Европа, Азия)'
        verbose_name_plural = 'Регионы (Европа, Азия)'


class Country(models.Model):
    region = models.ForeignKey(
        to=Region,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name='Регион',
        related_name='countries'
    )
    title = models.CharField(
        verbose_name='Название страны',
        max_length=255
    )
    icon = models.FileField(
        verbose_name='Иконка',
        upload_to='country_icons/'
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'


class From(models.Model):
    title = models.CharField(
        verbose_name='Название города',
        max_length=255
    )
    country = models.ForeignKey(
        to=Country,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name='Страна',
        related_name='froms'
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Откуда (Места из которых люди полетят)'
        verbose_name_plural = 'Откуда (Места из которых люди полетят)'
        ordering = ['-title']


class To(models.Model):
    title = models.CharField(
        verbose_name='Название',
        max_length=255
    )
    country = models.ForeignKey(
        to=Country,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name='Страна',
        related_name='to'
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Куда (Места в которые вы предлагаете полететь.)'
        verbose_name_plural = 'Куда (Места в которые вы предлагаете полететь.)'
        ordering = ['-title']


class Airline(models.Model):
    title = models.CharField(
        verbose_name='Название авиалинии',
        max_length=255
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Авиалиния'
        verbose_name_plural = 'Авиалинии'


class Tour(models.Model):
    class Cuurency(models.TextChoices):
        summ = 'sum', 'Сум'
        usd = 'usd', 'Доллар'

    class People(models.IntegerChoices):
        one = 1, '1'
        two = 2, '2'
        three = 3, '3'
        four = 4, '4'

    class Transfer(models.TextChoices):
        group = 'group', 'Групповой'
        individual = 'individual', 'Индивидуальный'

    face_image = models.ImageField(
        verbose_name='Главная фотография',
        upload_to='face_images/'
    )
    global_title = models.CharField(
        verbose_name='Главное название',
        max_length=255
    )
    mini_title = models.CharField(
        verbose_name='Второстепенное название/Мини описание',
        max_length=255,
        blank=True,
        null=True
    )
    created = models.DateTimeField(
        verbose_name='Создано',
        auto_now_add=True
    )
    currency = models.CharField(
        verbose_name='Валюта',
        max_length=255,
        choices=Cuurency.choices,
        default='usd'
    )
    start_sum = models.FloatField(
        verbose_name='От (начальная цена)',
        default=0,
        null=True,
        blank=True
    )
    amount = models.FloatField(
        verbose_name='Общая стоимость',
        default=0,
        null=True,
        blank=True
    )
    is_active = models.BooleanField(
        verbose_name='Активен ?',
        default=True
    )
    is_hot = models.BooleanField(
        verbose_name='Популярен ?',
        default=False
    )
    froms = models.ForeignKey(
        verbose_name='Откуда',
        to=From,
        on_delete=models.SET_NULL,
        null=True
    )
    to = models.ForeignKey(
        verbose_name='Куда',
        to=To,
        on_delete=models.SET_NULL,
        null=True
    )
    start_date = models.DateField(
        verbose_name='Дата начала',
        default=timezone.now(),
        null=True
    )
    end_date = models.DateField(
        verbose_name='Дата конца',
        default=timezone.now(),
        null=True
    )
    transfer = models.CharField(
        verbose_name='Трансфер',
        max_length=255,
        choices=Transfer.choices
    )
    airline = models.ForeignKey(
        to=Airline,
        verbose_name='Авиалиния',
        on_delete=models.SET_NULL,
        null=True
    )
    have_a_car = models.BooleanField(
        verbose_name='Аренда машины ?',
        default=False
    )
    have_a_boat = models.BooleanField(
        verbose_name='Аренда лодки ?',
        default=False
    )
    content = RichTextUploadingField(
        verbose_name='Контент',
        null=True
    )
    in_uzbekistan = models.BooleanField(
        verbose_name='По Узбекистану ?',
        default=False
    )
    tourists = models.SmallIntegerField(
        verbose_name='Туристы',
        choices=People.choices,
        null=True
    )

    def __str__(self):
        return self.global_title

    def clean(self):
        if self.amount and self.start_sum:
            raise ValidationError('Не может быть цены от и общей суммы в одном туре')

    class Meta:
        verbose_name = 'Тур'
        verbose_name_plural = 'Туры'


class Day(models.Model):
    tour = models.ForeignKey(
        to=Tour,
        verbose_name='Тур',
        related_name='days',
        on_delete=models.CASCADE
    )
    number = models.SmallIntegerField(
        verbose_name='День',
        default=1,
    )
    content = RichTextUploadingField(
        verbose_name='Контент'
    )

    def __str__(self):
        return f"{self.tour.global_title} | Day {self.number}"

    class Meta:
        verbose_name = 'День'
        verbose_name_plural = 'Дни'


class TourImage(models.Model):
    tour = models.ForeignKey(
        to=Tour,
        verbose_name='Тур',
        related_name='images',
        on_delete=models.CASCADE
    )
    image = models.ImageField(
        verbose_name='Фотография',
        upload_to='tour_images/'
    )

    def __str__(self):
        return self.image.url

    class Meta:
        verbose_name = 'Фотография тура'
        verbose_name_plural = 'Фотографии туров'


class Slider(models.Model):
    image = models.ImageField(
        verbose_name='Картинка',
        upload_to='slider_image/'
    )

    def __str__(self):
        return self.image.url

    class Meta:
        verbose_name = 'Слайдер'
        verbose_name_plural = 'Слайдер'


class HotelTo(models.Model):
    title = models.CharField(
        verbose_name='Название города',
        max_length=255
    )

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = 'Города в которых находятся отели.'
        verbose_name_plural = 'Города в которых находятся отели.'


class Hotel(models.Model):
    class People(models.IntegerChoices):
        one = 1, '1'
        two = 2, '2'
        three = 3, '3'
        four = 4, '4'

    to = models.ForeignKey(
        HotelTo,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name='Куда'
    )
    start_date = models.DateField(
        verbose_name='Дата начала',
        default=timezone.now(),
        null=True
    )
    end_date = models.DateField(
        verbose_name='Дата конца',
        default=timezone.now(),
        null=True
    )
    people_count = models.SmallIntegerField(
        verbose_name='Количество посетителей',
        choices=People.choices
    )
    title = models.CharField(
        verbose_name='Название отеля',
        max_length=255
    )
    amount = models.IntegerField(
        verbose_name='Цена',
        default=0
    )
    address = models.CharField(
        verbose_name='Адресс',
        max_length=255
    )
    image = models.ImageField(
        verbose_name='Фотография',
        upload_to='hotels/'
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Отель'
        verbose_name_plural = 'Отели'


class Question(models.Model):
    title = models.CharField(
        verbose_name='Заголовок',
        max_length=255
    )
    content = models.TextField(
        verbose_name='Контент'
    )
    image = models.ImageField(
        verbose_name='Фотография'
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Памятка для туристов'
        verbose_name_plural = 'Памятки для туристов'
