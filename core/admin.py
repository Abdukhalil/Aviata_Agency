from django.contrib import admin
from core.models import Tour, Day, TourImage, Slider, From, To, Country, Airline, Hotel, HotelTo, Question, Region
from tabbed_admin.admin import TabbedModelAdmin
from modeltranslation.admin import TranslationTabularInline, TabbedTranslationAdmin

from site_management.models import Visa


class TourImageInline(admin.TabularInline):
    model = TourImage
    min_num = 0
    extra = 1


class DayInline(TranslationTabularInline):
    model = Day
    min_num = 0
    extra = 1


@admin.register(Tour)
class TourAdmin(TabbedModelAdmin, TabbedTranslationAdmin):
    list_display = ['global_title', 'start_sum', 'amount', 'created', 'is_active', 'start_date', 'end_date']
    list_editable = ['is_active']

    tab_overview = (
        (None,  {
            'fields': ['face_image', 'currency', 'amount', 'start_sum',  'start_date', 'end_date', 'airline',
                       'froms', 'to', 'is_active', 'in_uzbekistan', 'have_a_car', 'have_a_boat',
                       'is_hot', 'global_title', 'mini_title', 'transfer', 'content', 'tourists']
        }),

    )
    tab_days = (
        DayInline,
    )
    tab_tour_images = (
        TourImageInline,
    )
    tabs = [
        ('Тур', tab_overview),
        ('Дни', tab_days),
        ('Фотографии', tab_tour_images),
    ]
    save_as = True

@admin.register(Airline)
class AirlineAdmin(TabbedTranslationAdmin):
    pass


@admin.register(From)
class FromAdmin(TabbedTranslationAdmin):
    pass


@admin.register(To)
class ToAdmin(TabbedTranslationAdmin):
    pass


@admin.register(Country)
class CountryAdmin(TabbedTranslationAdmin):
    pass

@admin.register(Region)
class RegionAdmin(TabbedTranslationAdmin):
    pass


@admin.register(Hotel)
class HotelAdmin(TabbedTranslationAdmin):
    pass


@admin.register(HotelTo)
class HotelToAdmin(TabbedTranslationAdmin):
    pass


@admin.register(Question)
class QuestionAdmin(TabbedTranslationAdmin):
    pass

@admin.register(Visa)
class QuestionAdmin(TabbedTranslationAdmin):
    pass
