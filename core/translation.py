from modeltranslation.translator import register, TranslationOptions

from site_management.models import Visa
from .models import Tour, Day, Question, HotelTo, Country, From, Airline, To, Hotel, Region


@register(Tour)
class NewsTranslationOptions(TranslationOptions):
    fields = ('global_title', 'mini_title',)


@register(Day)
class DayTranslationOptions(TranslationOptions):
    fields = ('content',)


@register(Question)
class DayTranslationOptions(TranslationOptions):
    fields = ('title', 'content',)


@register(HotelTo)
class HotelToTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(Country)
class CountryTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(From)
class FromTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(Airline)
class AirlineTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(To)
class ToTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(Hotel)
class HotelTranslationOptions(TranslationOptions):
    fields = ('title',)

@register(Visa)
class VisaTranslationOptions(TranslationOptions):
    fields = ('text',)

@register(Region)
class RegionTranslationOptions(TranslationOptions):
    fields = ('title',)
