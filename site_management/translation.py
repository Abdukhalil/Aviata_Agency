from modeltranslation.translator import register, TranslationOptions
from .models import BestTourText, AboutUsText, ContactUsText, ToursHeader, Workers, MainImage


@register(BestTourText)
class BestTourTextTranslationOptions(TranslationOptions):
    fields = ('title', 'content')


@register(AboutUsText)
class AboutUsTextTranslationOptions(TranslationOptions):
    fields = ('title', 'content')


@register(ContactUsText)
class ContactUsTextTranslationOptions(TranslationOptions):
    fields = ('content',)


@register(Workers)
class WorkersTranslationOptions(TranslationOptions):
    fields = ('title', 'role')



