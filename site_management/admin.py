from django.contrib import admin
from modeltranslation.admin import TranslationAdmin

from site_management.models import BestTourText, ContactUsText, AboutUsText, ToursHeader, Workers, MainImage


@admin.register(BestTourText)
# @admin.register(ContactUsText)
@admin.register(Workers)
class AdminModel(TranslationAdmin):
    pass

@admin.register(MainImage)
@admin.register(ToursHeader)
class ToursHeaderAdmin(admin.ModelAdmin):
    pass
