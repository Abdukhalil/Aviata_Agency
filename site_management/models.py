from django.db import models


class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj


class BestTourText(SingletonModel):
    title = models.CharField(
        verbose_name='Заголовок',
        max_length=255
    )
    content = models.TextField(
        verbose_name='Контент'
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Описание для самых популярных туров на глвной странице"
        verbose_name_plural = 'Описание для самых популярных туров на глвной странице'


class AboutUsText(SingletonModel):
    title = models.CharField(
        verbose_name='Заголовок',
        max_length=255
    )
    content = models.TextField(
        verbose_name='Контент'
    )
    photo = models.ImageField(
        verbose_name='Фотография',
        upload_to='about_us_photo/',
        null=True,
        blank=True
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Блог'
        verbose_name_plural = 'Блог'


class ContactUsText(SingletonModel):
    content = models.TextField(
        verbose_name='Контент'
    )

    class Meta:
        verbose_name = 'Свяжитесь с нами'
        verbose_name_plural = 'Свяжитесь с нами'


class ToursHeader(SingletonModel):
    photo = models.ImageField(
        verbose_name='Фотография',
        upload_to='headers/'
    )

    def __str__(self):
        return self.photo.url

    class Meta:
        verbose_name = 'Верхняя фотограция на странице'
        verbose_name_plural = 'Верхняя фотограция на странице'


class Visa(SingletonModel):
    text = models.TextField(
        verbose_name='Текст',
    )
    image = models.ImageField(
        verbose_name='Фотография'
    )

    def __str__(self):
        return self.image.url

    class Meta:
        verbose_name = 'Визовая поддержка (описание картинка)'
        verbose_name_plural = 'Визовая поддержка  (описание картинка)'


class Workers(models.Model):
    title = models.CharField(
        verbose_name='Имя',
        max_length=255
    )
    role = models.CharField(
        verbose_name='Роль',
        max_length=255
    )
    image = models.ImageField(
        verbose_name='Фотография'
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Наши специалисты'
        verbose_name_plural = 'Наши специалисты'


class MainImage(SingletonModel):
    image = models.ImageField(
        verbose_name='Фотография',
        upload_to='main_image/'
    )

    def __str__(self):
        return f"{self.image.url}"

    class Meta:
        verbose_name = 'Картинка на главной странице'
        verbose_name_plural = 'Картинка на главной странице'
